package com.carstore.carstore.repo;

import com.carstore.carstore.dto.AutomobilMarkaDto;
import com.carstore.carstore.model.Automobil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AutomobilRepository extends JpaRepository<Automobil, Integer> {

    @Query( value="SELECT * FROM automobil a INNER JOIN marka m ON m.id = a.marka_id", nativeQuery = true)
    List<Automobil> getAllAutomobils();
    @Query( value="SELECT * FROM automobil a INNER JOIN marka m ON m.id = a.marka_id WHERE m.ime LIKE :param% " +
            "OR a.godiste LIKE :param%", nativeQuery = true)
    List<Automobil> getAutomobilLike(@Param("param") String param);
}
