package com.carstore.carstore.repo;

import com.carstore.carstore.model.Marka;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarkaRepository extends JpaRepository<Marka, Integer> {
}
