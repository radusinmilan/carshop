package com.carstore.carstore.service;

import com.carstore.carstore.model.Marka;
import com.carstore.carstore.repo.MarkaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MarkaService {
    @Autowired
    MarkaRepository markaRepository;

    public List<Marka> getMarka(){
       return  markaRepository.findAll();
    }

}
