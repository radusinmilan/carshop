package com.carstore.carstore.service;

import com.carstore.carstore.model.Automobil;
import com.carstore.carstore.repo.AutomobilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Service
public class AutomobilService {

    @Autowired
    AutomobilRepository automobilRepository;


    public List<Automobil> getAllCars () {
        return automobilRepository.getAllAutomobils();
    }

    public void addCar (Automobil automobil) {
       automobilRepository.save(automobil);
    }

    public Automobil getCar (int id) {
        return automobilRepository.getOne(id);
    }

    public void deleteCar (int id) {
         automobilRepository.deleteById(id);
    }

    public List<Automobil>getAutomobilLikeParam(String param){
       return param!=null? automobilRepository.getAutomobilLike(param):automobilRepository.findAll();
    }
}
