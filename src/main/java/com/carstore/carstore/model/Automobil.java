package com.carstore.carstore.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Automobil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="marka_id", insertable=false, updatable=false)
    private Marka marka;
/*
    @ManyToOne
    @JoinColumn(name="karoserija_id", insertable=false, updatable=false)
    private Karoserija karoserija;

    @ManyToOne
    @JoinColumn(name="gorivo_id", insertable=false, updatable=false)
    private Gorivo gorivo;

    private Integer marka_id;
    private Integer karoserija_id;
    private Integer gorivo_id;
*/

    private Integer marka_id;
    private int cena;
    private int godiste;
    private String slika;
    public static final String COMPLEATE_PATH="C:\\Users\\Zoki\\Spring boot\\carstore\\src\\main\\resources\\static";
    public static final String IMAGE_PATH = "\\img\\";
    public static final String IMAGE_PATH_ERROR = "\\img\\error.jpg";

    public Automobil(Marka marka, Integer marka_id, int cena, int godiste, String slika) {
        this.marka = marka;
        this.marka_id = marka_id;
        this.cena = cena;
        this.godiste = godiste;
        this.slika = slika;
    }

    public Automobil() {

    }

    public Marka getMarka() {
        return marka;
    }

    public void setMarka(Marka marka) {
        this.marka = marka;
    }

    public Integer getMarka_id() {
        return marka_id;
    }

    public void setMarka_id(Integer marka_id) {
        this.marka_id = marka_id;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getGodiste() {
        return godiste;
    }

    public void setGodiste(int godiste) {
        this.godiste = godiste;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Automobil{" +
                "id=" + id +
                ", marka=" + marka +
                ", marka_id=" + marka_id +
                ", cena=" + cena +
                ", godiste='" + godiste + '\'' +
                ", slika='" + slika + '\'' +
                '}';
    }
}
