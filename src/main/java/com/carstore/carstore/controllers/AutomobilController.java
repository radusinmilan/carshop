package com.carstore.carstore.controllers;


import com.carstore.carstore.helpers.ImageHelper;
import com.carstore.carstore.model.Automobil;
import com.carstore.carstore.model.Marka;
import com.carstore.carstore.service.AutomobilService;
import com.carstore.carstore.service.MarkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Controller
public class AutomobilController {

    @Autowired
    AutomobilService automobilService;
    @Autowired
    MarkaService markaService;

    @GetMapping("/cars")
    public String getCars(String param, Model model) {
        List<Automobil> cars = param == null ?  automobilService.getAllCars() : automobilService.getAutomobilLikeParam(param);
        //List<Automobil> cars = automobilService.getAllCars();
        model.addAttribute("cars", cars);
        return "cars";
    }

   @GetMapping("/cars/addCar")
    public String carForm(Model model) {
      List<Marka> marke =  markaService.getMarka();
      model.addAttribute("marke", marke);
      model.addAttribute("automobil", new Automobil());
      return "form";
    }
    @RequestMapping(value="/cars/addCar", method = RequestMethod.POST)
    public String addCar(@ModelAttribute("automobil")  Automobil automobil, @RequestParam("imageFile") MultipartFile imageFile) {
        ImageHelper.uploadImage( automobil, imageFile, automobilService);
        automobilService.addCar(automobil);
        return "redirect:/cars";    }

        //EDIT TO DO IMAGE EDIT
    @RequestMapping("/cars/editCar/{id}")
    public String editCar(@PathVariable("id") int id, Model model) {
        List<Marka> marke =  markaService.getMarka();
        model.addAttribute("marke", marke);
        model.addAttribute("automobil", automobilService.getCar(id));
        return "form";
    }

    @RequestMapping("/cars/deleteCar/{id}")
    public String deleteCar(@PathVariable("id") int id) {
        ImageHelper.deleteImageFromImg(id,automobilService);
        automobilService.deleteCar(id);
        return "redirect:/cars";
    }

    @RequestMapping("/cars/search")
    public String searchCar(String param, Model model){
        List<Automobil> cars = automobilService.getAutomobilLikeParam(param);
        model.addAttribute("cars", cars);
        return "redirect:/cars";
    }
}
