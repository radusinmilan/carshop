package com.carstore.carstore.helpers;

import com.carstore.carstore.model.Automobil;
import com.carstore.carstore.service.AutomobilService;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageHelper {

    /**
     * id from model, automobil service object
     * delete image from img folder from app
     */
    public static void deleteImageFromImg(int id, AutomobilService automobilService) {
        Automobil automobil = automobilService.getCar(id);
        if (automobil == null){
            throw new RuntimeException("delete car failed");
        } else {
            File fileToDelete = new File(Automobil.COMPLEATE_PATH+automobil.getSlika());
            fileToDelete.delete();
            System.out.println("delete image path: " +Automobil.COMPLEATE_PATH+automobil.getSlika());
        }
    }

    /**
     * imageFile , image from add car form
     * Store image to folder img
     */
    public static void uploadImage(Automobil automobil, MultipartFile imageFile, AutomobilService automobilService) {
        automobil.setSlika(Automobil.IMAGE_PATH + imageFile.getOriginalFilename());
        try {
            String path="errorr.jpg";
            FileOutputStream output;
            final String imagePath = "C:\\Users\\Zoki\\Spring boot\\carstore\\src\\main\\resources\\static\\img\\"; //path
            if (imageFile.getOriginalFilename().equalsIgnoreCase("") || imageFile.getOriginalFilename() == null) {
                output = new FileOutputStream(imagePath+path);
                automobil.setSlika("/img/"+path);
                output.write(imageFile.getBytes());
            } else {
                output = new FileOutputStream(imagePath+imageFile.getOriginalFilename());
                output.write(imageFile.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
